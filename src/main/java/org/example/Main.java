package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class Main {
    public static void main(String[] args) {

        WebDriver driver = new ChromeDriver();

        driver.get("https://pastebin.com/");

        WebElement pasteCodeField = driver.findElement(By.id("postform-text"));
        pasteCodeField.sendKeys("git config --global user.name  \"New Sheriff in Town\"\n" +
                "            git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "            git push origin master --force\nr");



//Click AGREE
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        try {
            WebElement confButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[class=' css-47sehv']")));
            if (confButton.isDisplayed()) {
                confButton.click();
            }
        } catch (TimeoutException ignored) {
            // Handle the case where the "Agree" button doesn't appear within the specified wait time
            System.out.println("Agree button didn't appear within the specified wait time.");
        }

//Click Close banner
        try {
            Thread.sleep(5000); // 5000 milliseconds = 5 seconds
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Wait<WebDriver> wait2 = new WebDriverWait(driver, Duration.ofSeconds(20));

        try {
            WebElement banner = wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("hideSlideBanner")));
            if (banner.isDisplayed()) {

                banner.click();
            }
        } catch (TimeoutException ignored) {
            // Handle the case where the banner doesn't appear within the specified wait time
            System.out.println("Banner downside didn't appear within the specified wait time.");
        }

        //Choose Bash
        WebElement highlightDropdown = driver.findElement(By.id("select2-postform-format-container"));
        highlightDropdown.click();
        WebElement bashOption = driver.findElement(By.xpath("//li[text()='Bash']"));
        bashOption.click();

        //Choose 10 minutes
        WebElement expirationDropdown = driver.findElement(By.id("select2-postform-expiration-container"));
        expirationDropdown.click();
        WebElement tenMinutesOption = driver.findElement(By.xpath("//li[text()='10 Minutes']"));
        tenMinutesOption.click();

        //Name
        WebElement pasteNameField = driver.findElement(By.id("postform-name"));
        pasteNameField.sendKeys("how to gain dominance among developers");

        //Create button click
        WebElement createButton = driver.findElement(By.xpath("//button[@class='btn -big']"));
        createButton.click();
        System.out.println("Paste Created!");

        // Check for the page title to match the expected paste name/title
        WebDriverWait titleWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            titleWait.until(ExpectedConditions.titleContains("how to gain dominance among developers"));
            System.out.println("Page title matches paste name/title.");
        } catch (TimeoutException ignored) {
            System.out.println("Page title does not match paste name/title within the specified wait time.");
        }

// Wait for the Bash string
        WebElement bashButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/archive/bash']")));
// Check if the Bash button is present
        if (bashButton.isDisplayed()) {
            System.out.println("Syntax is set to Bash.");
        } else {
            System.out.println("Syntax is not set to Bash.");
        }

// Find the <div> element containing the text
        WebElement sourceElement = driver.findElement(By.cssSelector("div.source.bash"));

// Get the text content of the <div> element
        String actualText = sourceElement.getText();

// Expected text
        String expectedText = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "            git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "            git push origin master --force\n" +
                "r";

// Check if the actual text matches the expected text
        if (actualText.equals(expectedText)) {
            System.out.println("Text matches the expected text.");
        } else {
            System.out.println("Text does not match the expected text.");
        }


      driver.quit();
    }
}
